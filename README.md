# Email
Classes utilizadas:
- [PHPMailler](https://github.com/PHPMailer/PHPMailer)

## Informação:
Para que este sistema funcione corretamente, você deve ativar o suporte para aplicativos inseguros do Gmail acessando [Este link](https://www.google.com/settings/u/1/security/lesssecureapps) e selecionando a opção "ativar".

## Usando:

1. Configure o servidor de email.
        
      __Arquivo: config.inc.php__
      
        <?php
        
        include_once "lib/Email.class.php";
        
          Email::configurar([
          
              // Configurações do gmail
               'host'     => 'smtp.gmail.com'
              ,'port'     => 587
              ,'secure'   => 'tls'
          
              // Login do seu email no gmail
              ,'username' => 'joao@gmail.com' // Remetente do email
              ,'password' => 'senhaDoJoao'    // Senha do Remetente
          
              // Informações do email
              ,'name'     => "Joãozinho formulários" // Nome do seu site
              ,'subject'  => 'Você recebeu um novo formulário!' // Assunto do Email
              // ,'debug'    => 2     // Descomente isso para exibir logs, alertas e erros.
          ]);
2. Inclua o arquivo de classe localizado em __lib/Email.class.php__.
        
      __Arquivo: enviar.php__
        
        <?php
        
        include_once "lib/Email.class.php";
        
3. Instancie um novo `Email` passando o formulário e o email destinatário:

      __Arquivo: enviar.php__
        
        <?php
        
        include_once "lib/Email.class.php";
        
        $form = $_POST;
        $destinatario = "maria@gmail.com";
        $meuEmail = new Email($form, $destinatario);
        
4. Enviando o email:

    __Arquivo: enviar.php__

        <?php
        
        include_once "lib/Email.class.php";
        
        $form = $_POST; // Obtem o formulário via POST
        $destinatario = "maria@gmail.com";
        $meuEmail = new Email($form, $destinatario);
        
        // Enviando o email...
        if($meuEmail->enviar()){
            echo "Email enviado com sucesso!";
        }else{
            echo "Falha ao enviar o email!";
        }
        
## Referências:

- [Documentação do PHPMailler](https://github.com/PHPMailer/PHPMailer/wiki)
- [
Enviar e-mails pelo PHP usando o PHPMailer](http://blog.thiagobelem.net/enviar-e-mails-pelo-php-usando-o-phpmailer) - Blog Thiago Belém

### Criado por [Salomão Neto](http://fb.com/salomaosnff)
Visite e siga meu [Codepen](http://codepen.io/salomaosnff)