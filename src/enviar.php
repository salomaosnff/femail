<?php
/**
 * Created by PhpStorm.
 * User: sallon
 * Date: 14/11/16
 * Time: 22:33
 */

include_once "config.inc.php";
$form = $_POST;

if(empty($form)){
    echo "Você deve preencher o formulário antes de envia-lo!";
}else{
    $meuEmail = new FEmail($form, "EMAIL_DO_DESTINATÁRIO");

    if($meuEmail->enviar()){
        echo "Formulário enviado com sucesso!";
    }else{
        echo "Falha ao enviar o formulário.";
    }
}