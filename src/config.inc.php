<?php
/**
 * Created by PhpStorm.
 * User: sallon
 * Date: 14/11/16
 * Time: 22:23
 */

include_once "lib/FEmail.class.php";

FEmail::configurar([

    // Configurações do gmail
     'host'     => 'smtp.gmail.com'
    ,'port'     => 587
    ,'secure'   => 'tls'

    // Login do seu email no gmail (remetente)
    ,'username' => 'SEU_EMAIL_DO_GMAIL'
    ,'password' => 'SUA_SENHA_DO_GMAIL'

    // Informações do email
    ,'name'     => "Informes do seu site"
    ,'subject'  => 'Você recebeu um novo formulário!'
//    ,'debug'    => 2     // Descomente isso para exibir logs, alertas e erros.
]);