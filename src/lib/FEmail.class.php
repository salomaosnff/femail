<?php

/**
 * Created by PhpStorm.
 * User: sallon
 * Date: 14/11/16
 * Time: 21:28
 */
require "phpMailler/class.phpmailer.php";
require "phpMailler/class.smtp.php";

class FEmail{
    private static $config = [];    // Configurações
    private $form;  // Formulário ($_GET ou $_POST)
    private $mail;  // Email do PHPMailler

    /**
     * Construtor do Email, não precisa mecher em nada aqui.
     * @param $form
     * @param $para
     */
    public function __construct($form, $para){
        $this->form = $form;    // Passa o form pro objeto

        $mail = new PHPMailer;                     // Novo email
        $mail->isSMTP();                           // É SMTP
        $mail->SMTPDebug = self::$config['debug']; // Exibe mensagens de Log?
        $mail->Debugoutput = 'html';               // Saída do Debug em HTML
        $mail->Host = self::$config['host'];       // Host do servidor do email

        $mail->SMTPOptions = [                     // Opções do SMTP
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ];

        $mail->Port = self::$config['port'];                                // Porta do servidor de Email
        $mail->SMTPSecure = self::$config['secure'];                        // Tipo de segurança
        $mail->SMTPAuth = true;                                             // Usa autenticação?
        $mail->Username = self::$config['username'];                        // Login do servidor de email
        $mail->Password = self::$config['password'];                        // Senha do servidor de email
        $mail->setFrom(self::$config['username'], self::$config['name']);   // Quem enviou o Email?
        $mail->addAddress($para);                                           // Para quem é o Email?
        $mail->Subject = self::$config['subject'];                          // Qual o assunto do email?
        $mail->CharSet = "utf-8";                                           // Codificação de caracteres
        $mail->msgHTML($this->gerarTabela());                               // Conteúdo HTML do email
        $mail->AltBody = $this->gerarTexto();                               // Conteúdo em Texto Plano do Email

        $this->mail = $mail;                                                // Passa o email para este objeto
    }

    public function enviar(){
        $enviou = $this->mail->send();  // O Email foi enviado?
        $this->mail->smtpClose();       // Feche a conexão com o servidor.

        return $enviou;                 // Retorna se o email foi enviado (true ou false)

    }

    /**
     * Gera uma tabela (em HTML) com as informações do formulário,
     * Pode mecher aqui.
     * @return string
     */
    public function gerarTabela(){
        $tabela = "<table>"; // Inicio tabela

            // Iteração dos campos do formulário
            foreach ($this->form as $campo => $valor){
                $tabela .= "<tr>";                 // Inicio Linha
                $tabela .=    "<td>$campo: </td>"; // Coluna Nome do campo
                $tabela .=    "<td>$valor</td>";   // Coluna valor do campo
                $tabela .= "</tr>";                // Fim linha
            }

        $tabela .= "</table>"; // Fim tabela

        return $tabela; // Retorna a tabela
    }

    /**
     * Gera uma mensagem (em texto) com as informações do formulário,
     * Pode mecher aqui.
     * @return string
     */
    public function gerarTexto(){
        $mensagem = "Dados do formulário: ";
        $mensagem .= json_encode($this->form); // Coloquei en JSON mas pode mudar

        return $mensagem; // retorna o texto
    }

    /**
     * Retorna erros de envio
     * Não precisa mecher
     * @return string
     */
    public function getError(){
        return $this->mail->ErrorInfo; // retorna os erros
    }

    /**
     * Configura a conexão com o servidor de email
     * Não precisa mecher aqui.
     * @param $configuracoes
     */
    public static function configurar($configuracoes){
        $defaults = [
            'host'    => null,
            'porta'   => null,
            'email'   => null,
            'senha'   => null,
            'nome'    => "Anônimo",
            'assunto' => "Sem Assunto",
            'debug'   => 0
        ];

        // Mescla Arrays
        self::$config = array_merge($defaults, $configuracoes);
    }
}